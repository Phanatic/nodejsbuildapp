/**
 * Created by kay on 29.10.13.
 */

module.exports = {
    /**
     * clear the console
     */
    clear: function () {
        process.stdout.write('\u001B[2J\u001B[0;0f');
    }
};