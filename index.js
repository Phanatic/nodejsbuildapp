/**
 * buildTool for iStart for chrome
 *
 *
 * @license: Gpl license TODO: insert here the GPL license with link to the GPL license online
 * @author: Kay Schneider <kayoliver82@gmail.com>
 *
 * @type {buildTool|exports|*}
 */

/**
 * TODO: add options direct via comandline arguments to the nodejs script
 * @type {buildTool|exports|*}
 */
var build  = require('./worker/buildTool.js');
var checkParams = require('./systools/commandparameters.js');
var buildMaster = new build();

var debug = false;
/**
 * at index 2 will be the parameters from the console
 */
//read the systemparameters and give this options to the app itself the application will now detect what is going on in the system
var params = checkParams(process.argv);

if(params === undefined) {
    params = {};
}



buildMaster.init(params);
buildMaster.getAddonPackages();
buildMaster.buildFileObjects();

