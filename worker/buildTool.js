/**
 * Created by kay on 16.10.13.
 */
/**
 * buildTool for istar
 * Author: Kay Schneider <kayoliver82@gmail.com>
 * master mind of the build process. Manage all the small kinds..
 * adds the right files to the processors builds the new js config filres
 * html files and so on
 */
var istartDir = '../../istartnewtab/';
var cpDir  = '../istartnewtab/';
var globalOutFolder = './output/js/';
var globalTempFolder = './output/js/';
var htmlOut = './output/html/';

var jsFiles = require('./jsFiles');
var cssFiles = require('./cssFiles');
var readFiles = require('./readFiles');
//add the config from the development directory
var bootApp = require(istartDir  + '/js/boot/config.js');
var rmDir = require('./rmDir');
var path = require('path');
var sys = require('sys');
var fs = require('fs');
var parseFile = require('./parseFile') ;
var folderCopy = require('./folderCopy');
var cp = require('child_process');


var buildTool = function() {
    this.coreFiles;
    this.cssFiles;
    this.staticFolders;
    this.packages= [];
    this.processesReady = 0;
    this.packReady = [];
    this.fileProcessor = [];//container for all file processors
    this.debug=false; //debug will be used to minify or not minify the sources for debugging the application
    this.options=[];
};

buildTool.prototype.startTime = function () {
    this.timeStart = +new Date().getTime();
};

buildTool.prototype.getRunTime = function () {
    this.timeEnd = +new Date().getTime();
    var time =  ( this.timeEnd - this.timeStart ) / 1000 ;
    this.parseHtml();
    this.copyStatic();
    sys.puts( "script is running in : " + time + ' seconds');
};

buildTool.prototype.init = function(options) {

        this.options = options;
        if(typeof ( this.options.debug ) ==="undefined" ) {
            this.options.debug = false;
        }
        this.startTime();

        this.coreFiles = bootApp.bootstrapApp.getJsFiles();
        this.cssFiles = bootApp.bootstrapApp.getCssFiles();
        this.staticFolders = bootApp.bootstrapApp.getStaticFolders();
};

/**
 * get some options values direct from an array
 */
buildTool.prototype.getOptionValue = function (key) {
    for(item in this.options) {
        if(this.options[item][0] === key) {
            //return the falue
            return this.options[item][1];
        }
    }
    //return false by default
    return false;
};


buildTool.prototype.buildFileObjects = function () {
    if(this.getOptionValue('copystatic') === true) {
        this.getRunTime();
        return true;
    } else {
        rmDir(path.resolve( globalOutFolder + '/') );
    }

    var debug = this.getOptionValue('debug');
    var n = cp.fork(__dirname + '/fileSubWorker.js');
    ( function(that,n) {
        n.on('message', function(m) {
            that.checkmessage(m);
        });
    })(this,n);

    n.send({
        message: 'init',
        config: {
        type:'css',
        init: {
            files:this.cssFiles,
            packname:'istartcss',
            tempfolder: globalTempFolder,
            outFolder:globalOutFolder,
            debug:debug
        }
    }});

    this.fileProcessor.push(n);

    var js = cp.fork(__dirname + '/fileSubWorker.js');

    ( function(that,js) {
        js.on('message', function(m) {
            that.checkmessage(m);
        });
    })(this,js);
    js.send({
        message: 'init',
        config: {
        type:'js',
        init: {
            files:this.coreFiles,
            packname:'istartJs',
            tempfolder: globalTempFolder,
            outFolder:globalOutFolder,
            debug:debug
        }
    }});
    this.fileProcessor.push(js);

    for(var i in this.packages) {
        var packProcessorSub = cp.fork(__dirname + '/fileSubWorker.js');
        ( function(that,packProcessorSub) {
            packProcessorSub.on('message', function(m) {
              that.checkmessage(m);
            });
        })(this, packProcessorSub);
        packProcessorSub.send({
            message: 'init',
            config: {
            type:'js',
            init: {
                files:this.packages[i].jsfiles,
                packname:this.packages[i].package,
                tempfolder: globalTempFolder,
                outFolder:globalOutFolder,
                debug:debug
            }
        }});
        this.fileProcessor.push(packProcessorSub);
    }

};


/**
 * check if all the files are ready
 */
buildTool.prototype.checkAllReady = function (e) {
    this.packReady.push(e);
    this.processesReady++;
    if(this.processesReady === this.fileProcessor.length) {
        this.getRunTime();
    }

};


buildTool.prototype.checkmessage = function (message) {
    //child.kill([signal]);

    switch(message.message){
        case 'filesready':
            this.checkAllReady(message);
            break;
        case 'error':
            /*TODO: replace error log with an buildStat Reporting log*/
            console.log(message.error);
            this.checkErrorType(message);
            break;
    }
} ;

buildTool.prototype.checkErrorType = function(message) {
    switch(message.type) {
        case 'kill':
            /*INFO:if this is an critical error from an child process stop all processes and output an message to the user*/
            sys.puts("start stop child processes with SIGHUP signal");
            //make an blocking call
            var killResult = this.killAllChilds();
            sys.puts("Critical message from an child process. Stop build process. Take a look at the error message above");
            break;
    }
};

buildTool.prototype.killAllChilds = function () {
    for(var i in this.fileProcessor) {
        this.fileProcessor[i].kill('SIGHUP');
    }
    return true;
};





buildTool.prototype.getAddonPackages = function () {
        var addOnConfig = require( istartDir + '/js/addons/config.js');
        this.packages.push(addOnConfig);

};

/**
 * use the google closurecompiler to shrink the filessize of the
 * js files
 * after that move all with closure compiler to the new directory
 */
buildTool.prototype.compileFiles = function () {

};

/**
 * coping all the static files in the folders alround here ;)
 */
buildTool.prototype.copyStatic = function () {
    var folders= bootApp.bootstrapApp.getStaticFolders();
    console.log(folders, "folders");
    for(var i in folders) {

        console.log(  cpDir  + folders[i]);
        console.log(cpDir + 'html/' + folders[i]);

        folderCopy(cpDir + 'html/' + folders[i], globalOutFolder + '/../', folders[i].replace('../',''));

    }


    var files =bootApp.bootstrapApp.getStaticRootFiles();
    for(var i in files) {
        fs.createReadStream(cpDir + files[i]).pipe(fs.createWriteStream(globalOutFolder + '/../' + files[i]) ) ;
    }

};

buildTool.prototype.parseHtml = function () {
    var htmlFiles = bootApp.bootstrapApp.getHtmlFiles();
    //parse all the files with an parserObject
    for(var i in htmlFiles) {
        var parser = new parseFile();
        parser.init(htmlFiles[i].file,htmlFiles[i].folder,this.packReady,htmlOut);
        parser.readFileContents();
    }

};




module.exports = buildTool;

