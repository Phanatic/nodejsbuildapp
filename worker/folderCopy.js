/**
 * Created by kay on 17.10.13.
 */
/**
 * folder copy
 * Author: Kay Schneider <kayoliver82@gmail.com>
 **/

var ncp = require('ncp').ncp;

var folderCopy = function (folder,globalOut,folderOnlyName) {
    /**
     * copys an folder from its current location to globalOut/folderName
     */

    ncp.limit = 16;

    ncp(folder, globalOut + folderOnlyName , function (err) {
        if (err) {
            return console.error(err);
        }
        console.log('done!');
    });

};


module.exports = folderCopy;

