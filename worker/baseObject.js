var baseObject = function() {

    this.addEventListener = function(eventstring,callback,optionalObject) {

        if(typeof(this.listener) == "undefined") {
            this.listener = [];
        }
        var listener =  {
            'event':eventstring,
            'callback':callback,
            'applyme':optionalObject
        };
        this.listener.push(listener);
    };

    this.throwEvent = function (eventName, optionalData) {
        for(var item in this.listener) {
            listitem = this.listener[item];
            //call the callback if the event is listen the same like the actual
            if(listitem.event === eventName) {
                listitem.callback.apply(listitem.applyme,[optionalData]);
            }
        }
    };
};

module.exports = baseObject;