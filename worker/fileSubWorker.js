/**
 *
 * @type {exports|*}
 */

var sys = require('sys');
var jsFiles = require('./jsFiles');
var cssFiles = require('./cssFiles');

process.on('message', function(m,data) {
    switch(m.message) {
        case 'init' :
            switch(m.config.type) {
                case 'js':
                    subBuild.buildJs(m.config.init);
                    break;
                case 'css':
                    subBuild.buildCss(m.config.init);
                    break;
            }
            break;
    }
});


var subBuild = {};

subBuild.buildJs = function (config) {
    var processor = new jsFiles();
    subBuild.initProcessor(processor,config);
};

subBuild.initProcessor = function(processor,config) {
    processor.init(config.files,config.packname,config.tempfolder, config.outFolder,config.debug);
    subBuild.start(processor);
};

subBuild.buildCss = function (config) {
    var processor = new cssFiles();
    subBuild.initProcessor(processor,config);
};

subBuild.start = function (processor) {
    subBuild.processor = processor;
    subBuild.processor.addEventListener('filesready', function(e) {
        //when receive the ready event send message
        process.send({message:'filesready', pack:e,fileName:subBuild.processor.getTag()})
    });
    //start the work
    subBuild.processor.start();
};

